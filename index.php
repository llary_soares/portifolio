<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Portifólio</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/business-frontpage.css" rel="stylesheet">
  <meta http-equiv='X-UA-compatible' content='IE=edge, chrome=1'/>
  <link rel='stylesheet' href='uikit/css/uikit.min.css' />
  <link rel='stylesheet' href='uikit/css/css2.css' />
  <style>
    @media only screen and (max-width: 600px){
      .mobile-hide{
        display: none !important;
      }
    }
    @media only screen and (min-width: 400px){
      .desktop-hide{
        display: none !important;
      }
    }
  </style>

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="#">Laryssa Soares</a>
    </div>
  </nav>

  <!-- Header -->
  <header class="py-5 mb-5" style="background-color:#FFEBCD;">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-lg-12">
          <div class="uk-flex-middle" uk-grid>
            <div class="uk-width-2-3@m">
               <div class="row">
                  <h2>Quem sou?</h2>
                  <hr>
                  <div class="desktop-hide" style="margin: 7px;"><p class="uk-text-justify">Meu nome é Laryssa Soares, tenho 16 anos, estou cursando o ensino médio integrado ao curso técnico em informática na escola Paulo Petrola. Durante o decorrer do curso mudei completamente minha visão sobre a informática, principalmente na área de desenvolvimento, de alguma forma o que me tirava tanto o sono no primeiro ano do curso, se tornou um hobby e algo que me dá prazer em fazer. Por isso, pretendo seguir carreira como desenvolvedora fullstack, pois programo para back-end e front-end, entretanto não nego minha preferência pela parte do back-end! A seguir um pouquinho da minha história no curso técnico.</p></div>
                  <div class="mobile-hide"><p class="uk-text-justify">Meu nome é Laryssa Soares, tenho 16 anos, estou cursando o ensino médio integrado ao curso técnico em informática na escola Paulo Petrola. Durante o decorrer do curso mudei completamente minha visão sobre a informática, principalmente na área de desenvolvimento, de alguma forma o que me tirava tanto o sono no primeiro ano do curso, se tornou um hobby e algo que me dá prazer em fazer. Por isso, pretendo seguir carreira como desenvolvedora fullstack, pois programo para back-end e front-end, entretanto não nego minha preferência pela parte do back-end! A seguir um pouquinho da minha história no curso técnico.</p></div>
                  <p uk-margin>
                    <div class="desktop-hide" style="margin-left:10px;">
                      <a class="uk-button uk-button-default" href="#modal-group-1" uk-toggle style="background-color:white;">1º ano</a>
                      <a class="uk-button uk-button-default" href="#modal-group-2" uk-toggle style="background-color:white;">2º ano</a>
                      <a class="uk-button uk-button-default" href="#modal-group-3" uk-toggle style="background-color:white;">3º ano</a>
                    </div>
                    <div class="mobile-hide">
                      <a class="uk-button uk-button-default" href="#modal-group-1" uk-toggle style="background-color:white;">1º ano</a>
                      <a class="uk-button uk-button-default" href="#modal-group-2" uk-toggle style="background-color:white;">2º ano</a>
                      <a class="uk-button uk-button-default" href="#modal-group-3" uk-toggle style="background-color:white;">3º ano</a>
                    </div>
                  </p>
                  <div id="modal-group-1" uk-modal>
                    <div class="uk-modal-dialog">
                        <button class="uk-modal-close-default" type="button" uk-close></button>
                        <div class="uk-modal-header">
                            <h2 class="uk-modal-title">1º ano</h2>
                        </div>
                        <div class="uk-modal-body">
                            <p>No primeiro ano eu não tinha muita noção do que realmente era o curso técnico em informática, jurava que iria estudar só o pacote Office, porém em agosto de 2017, o Clemilton me apresenta o que seria a pior disciplina que eu já estudei em toda minha vida, Lógica de Programação, eu tive que me esforçar muito, chorei muito também, tinha certeza que não ia conseguir, até que consegui. Passei sem ir para recuperação e me tornei monitora do Clemilton (por livre e espontânea pressão). E assim começa minha história de amor com o desenvolvimento.</p>
                        </div>
                        <div class="uk-modal-footer uk-text-right">
                            <button class="uk-button uk-button-default uk-modal-close" type="button">Cancelar</button>
                            <a href="#modal-group-2" class="uk-button uk-link-reset" uk-toggle style="background-color:#FFEBCD;">Próximo</a>
                        </div>
                    </div>
                </div>
                <div id="modal-group-2" uk-modal>
                    <div class="uk-modal-dialog">
                        <button class="uk-modal-close-default" type="button" uk-close></button>
                        <div class="uk-modal-header">
                            <h2 class="uk-modal-title">2º ano</h2>
                        </div>
                        <div class="uk-modal-body">
                            <p>No segundo ano, com outra mentalidade e mais confiante, fui me apaixonando cada vez mais por esse universo que é programar, aquela felicidade de concluir um projeto, de todos os códigos executarem sem erros, dá sentido a todos os desafios que são enfrentados no percurso. Com o trabalho de PHP no final de 2018, eu tive a  certeza de que realmente era aquilo que eu queria fazer durante a minha vida.</p>
                        </div>
                        <div class="uk-modal-footer uk-text-right">
                          <div class="desktop-hide">
                            <a href="#modal-group-1" class="uk-button uk-link-reset" uk-toggle style="background-color:#FFEBCD; margin-left:2px;">Anterior</a>
                            <a href="#modal-group-3" class="uk-button uk-link-reset" uk-toggle style="background-color:#FFEBCD;">Próximo</a>
                            <button class="uk-button uk-modal-close uk-width-1-1 uk-margin-small-bottom" type="button" style="background-color:white;">Cancelar</button>
                          </div>
                          <div class="mobile-hide">
                            <button class="uk-button uk-button-default uk-modal-close" type="button">Cancelar</button>
                            <a href="#modal-group-1" class="uk-button uk-link-reset" uk-toggle style="background-color:#FFEBCD;">Anterior</a>
                            <a href="#modal-group-3" class="uk-button uk-link-reset" uk-toggle style="background-color:#FFEBCD;">Próximo</a>
                          </div>
                        </div>
                    </div>
                </div>
                <div id="modal-group-3" uk-modal>
                    <div class="uk-modal-dialog">
                        <button class="uk-modal-close-default" type="button" uk-close></button>
                        <div class="uk-modal-header">
                            <h2 class="uk-modal-title">3º ano</h2>
                        </div>
                        <div class="uk-modal-body">
                            <p>Agora, no terceiro ano, um novo mundo de incertezas surge, o estágio, o mercado de trabalho, o ENEM, faculdade e tudo isso causa uma grande pressão. Por isso, a todo momento me mantenho firme nos meus objetivos, faço o possível para realizar o que quero, sempre dando meu melhor.</p>
                        </div>
                        <div class="uk-modal-footer uk-text-right">
                            <button class="uk-button uk-button-default uk-modal-close" type="button">Cancelar</button>
                            <a href="#modal-group-2" class="uk-button uk-link-reset" uk-toggle style="background-color:#FFEBCD;">Anterior</a>
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="uk-width-1-3@m uk-flex-first">
              <div class="desktop-hide" style="margin-left: 40px"><img class="uk-border-circle" src="uikit/eu.png" width="250" height="400" alt="Border circle"></div>
              <div class="mobile-hide"><img class="uk-border-circle" src="uikit/eu.png" width="250" height="400" alt="Border circle"></div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </header>

  <!-- Page Content -->
  <div class="container">

    <div class="row">
      <div class="col-md-8 mb-5">
        <div class="mobile-hide">
          <h2>Algumas Informações</h2>
          <hr>
          <span class='uk-icon uk-margin-small-right' uk-icon='icon: chevron-right'></span><a id="link" href="" class="uk-button uk-button-text uk-text-small">Conhecimentos intermediários em PHP, MySQL, HTML, CSS e Framework Uikit.</a><br><br>
          <span class='uk-icon uk-margin-small-right' uk-icon='icon: chevron-right'></span><a id="link" href="" class="uk-button uk-button-text uk-text-small">Conhecimentos básicos em Java, JavaScript, JavaWeb, C, C++ e Python.</a><br><br>
          <span class='uk-icon uk-margin-small-right' uk-icon='icon: chevron-right'></span><a id="link" href="" class="uk-button uk-button-text uk-text-small">Presença no Frontin 2018.</a><br><br>
          <span class='uk-icon uk-margin-small-right' uk-icon='icon: chevron-right'></span><a id="link" href="" class="uk-button uk-button-text uk-text-small">Participação na segunda fase da Olimpíada Cearense de Informática em 2018.</a><br><br>
          <span class='uk-icon uk-margin-small-right' uk-icon='icon: chevron-right'></span><a id="link" href="" class="uk-button uk-button-text uk-text-small">Formação de Jovens Empreendedores.</a><br><br>
          <span class='uk-icon uk-margin-small-right' uk-icon='icon: chevron-right'></span><a id="link" href="" class="uk-button uk-button-text uk-text-small">Projeto Aluno Educador.</a><br><br>
        </div>
        <div class="desktop-hide">
          <h2>Algumas Informações</h2>
          <hr>
          <ul>
            <li>Conhecimentos intermediários em PHP, MySQL, HTML, CSS e Framework Uikit.</li><br>
            <li>Conhecimentos básicos em Java, JavaScript, JavaWeb, C, C++ e Python.</li><br>
            <li>Presença no Frontin 2018.</li><br>
            <li>Participação na segunda fase da Olimpíada Cearense de Informática em 2018.</li><br>
            <li>Formação de Jovens Empreendedores.</li><br>
            <li>Projeto Aluno Educador.</li><br>
          </ul>
        </div>
      </div>
      <div class="col-md-4 mb-5">
        <h2>Redes Sociais</h2>
        <hr>
        <span class='uk-icon uk-margin-small-right' uk-icon='icon:whatsapp' style="color:green"></span>(85) 98958-2047<br><br>
        <span class='uk-icon uk-margin-small-right' uk-icon='icon:instagram' style="color:purple"></span>@llary_ssoares<br><br>
        <span class='uk-icon uk-margin-small-right' uk-icon='icon:google' style="color:red"></span>laryssassoares14@gmail.com<br><br>
        <img src="gitlab.png"> @llary_soares
      </div>
    </div>
    <!-- /.row --> 

    <div class="row">
      <div class="col-md-4 mb-5">
        <div class="card h-100">
            <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow="animation: scale">
              <ul class="uk-slideshow-items">
                <li>
                  <img src="uikit/php1.png" alt="" uk-cover>
                </li>
                <li>
                  <img src="uikit/php2.png" alt="" uk-cover>
                </li>
                <li>
                  <img src="uikit/php3.png" alt="" uk-cover>
                </li>
                <li>
                  <img src="uikit/php4.png" alt="" uk-cover>
                </li>
                <li>
                  <img src="uikit/php5.png" alt="" uk-cover>
                </li>
                <li>
                  <img src="uikit/php6.png" alt="" uk-cover>
                </li>
                <li>
                  <img src="uikit/php7.png" alt="" uk-cover>
                </li>
              </ul>
              <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
              <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
            </div>
          <div class="card-body">
            <h4 class="card-title">Meu Mundo</h4>
            <p class="card-text uk-text-justify">O site em questão é um CMS, no qual uma suposta blogueira viajante posta suas experiências em diferentes lugares. O site foi feito em PHP com conexão com o banco de Dados (MySQL) e o front foi feito com Ajax, HTML, CSS e o framework Uikit. O administrador cadastrado é "laryssa" e a senha é "1234".</p>
          </div>
          <div class="card-footer">
            <?php
                echo"<a href='site/PDO/index.php' class='btn btn-light' style='background-color:#FFEBCD;'>Visite o site!</a>";
            ?>
          </div>
        </div>
      </div>
      <div class="col-md-4 mb-5">
        <div class="card h-100">
          <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow="animation: scale">
              <ul class="uk-slideshow-items">
                <li>
                  <img src="uikit/money2.png" alt="" uk-cover>
                </li>
                <li>
                  <img src="uikit/money1.png" alt="" uk-cover>
                </li>
                <li>
                  <img src="uikit/money3.png" alt="" uk-cover>
                </li>
                <li>
                  <img src="uikit/money4.png" alt="" uk-cover>
                </li>
                <li>
                  <img src="uikit/money5.png" alt="" uk-cover>
                </li>
                <li>
                  <img src="uikit/money6.png" alt="" uk-cover>
                </li>
              </ul>
              <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
              <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
            </div>

          <div class="card-body">
            <h4 class="card-title">Money</h4>
            <p class="card-text uk-text-justify">O software a cima foi feito para que clientes hipotéticos pudessem organizar suas finanças, disponibilizando de algumas funcionalidades básicas e de fácil utilização. Ele foi feito em PHP com conexão com o banco de dados (MySQL) e o front foi elaborado com Ajax, HTML, CSS e o framework Uikit.</p>
          </div>
          <div class="card-footer">
            <?php
                echo"<a href='money/home.php' class='btn btn-light' style='background-color:#FFEBCD;'>Visite o site!</a>";
            ?>
          </div>
        </div>
      </div>
      <div class="col-md-4 mb-5">
        <div class="card h-100">
          <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow="animation: scale">
              <ul class="uk-slideshow-items">
                <li>
                  <img src="uikit/js1.png" alt="" uk-cover>
                </li>
                <li>
                  <img src="uikit/js2.png" alt="" uk-cover>
                </li>
                <li>
                  <img src="uikit/js3.png" alt="" uk-cover>
                </li>
              </ul>
              <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
              <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
            </div>
          <div class="card-body">
            <h4 class="card-text">América do Sul</h4>
            <p class="card-text uk-text-justify">O site mostrado anteriormente se trata de um trabalho realizado na disciplina de JavaScript, com a finalidade de testar vários plugins e fazer uma página bastante interativa. Na sua elaboração foi utilizado JavaScript, diversos plugins, framework Uikit, HTML e CSS.</p>
          </div>
          <div class="card-footer">
            <a href="trabalhojs/uikit/brasil.html" class='btn btn-light' style='background-color:#FFEBCD;'>Visite o site!</a>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Laryssa Soares, desenvolvedora fullstack.</p>
    </div>
    <!-- /.container -->
  </footer>
  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src='uikit/js/uikit.min.js'></script>
  <script src='uikit/js/uikit-icons.min.js'></script>

</body>

</html>
